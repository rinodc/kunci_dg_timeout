String kunci = "123456"; // ini passwordnya

String input = "";
int counterSalah = 0;

int batasGagal = 5; // maksimal kegagalan
int waktuTunggu = 5; //menit

//Reset
void(* resetFunc) (void) = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Masukkan kode!");
  while (counterSalah < batasGagal)
  {
    while (!Serial.available()); // tunggu hingga ada kode masuk
    if (Serial.available())
    {
      input = Serial.readString();
    }
    if (input == kunci)
    {
      Serial.println("Berhasil Terbuka");

      break;
    }
    else
    {
      Serial.println("Salah!");
      counterSalah++;
      Serial.println(String(counterSalah) + " kali");
    }


  }

  if (counterSalah >= 5)
  {
    Serial.println("Gagal " + String(batasGagal) + " kali ");
    Serial.println("Tunggu " + String(waktuTunggu) +  " menit");
    for (int i = 0; i < waktuTunggu; i++)
    {
      delay(60000);
    }

    resetFunc();//RESET SYSTEM setelah 5 menit akan reset
  }



}

void loop() {
  Serial.println("SISTEM BERJALAN");
  delay(1000);
}
